import React from 'react';
import { useSelector } from 'react-redux';

function WatchLater() {
  const watchLaterList = useSelector(state => state.data.watchLater);
  
  const handleCardClick = (imdbUrl) => {
    window.open(imdbUrl, '_blank');
  };

  return (
    <div className="container">
      <h2>Watch Later List</h2>
      <div className="row">
        {watchLaterList.map(movie => (
          <div key={movie.id} className="col-12 col-md-6 col-lg-3 mb-3">
            <div className="card">
              <img src={movie.image} className="card-img-top" alt={movie.movie} />
              <div className="card-body">
                <h5 className="card-title">{movie.movie}</h5>
                <p>Rating: {movie.rating}</p>
                <button onClick={() => handleCardClick(movie.imdb_url)} className="btn btn-primary" > Watch Now </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default WatchLater;
