import React from 'react'
import Navbar from './Navbar'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import WatchLater from './WatchLater';
import Movies from './Movies';


export default function Entry() {
  return (
    <div>
      <Router>
      <Navbar/>
        <Routes>
          <Route path="/" element={<Movies/>} /> 
          <Route path="/movies" element={<Movies/>} /> 
          {/* <Route path="/page/:pageNumber" element={<Movies/>} />  */}
          <Route path="/WatchLater" element={<WatchLater/>} /> 
        </Routes>  
      </Router>
    </div>
  )
}
