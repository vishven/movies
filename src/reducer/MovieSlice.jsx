import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchMovies = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get('https://dummyapi.online/api/movies');
      dispatch(setMovies(response.data));
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };
};

const movieSlice = createSlice({
  name: 'movies',
  initialState: {
    movies: [],
    watchLater: [],
    status: 'idle',
    error: null,
  },
  reducers: {
    setMovies: (state, action) => {
      state.movies = action.payload;
      state.status = 'succeeded';
    },
    addToWatchLater: (state, action) => {
      state.watchLater.push(action.payload);
    },
  },
});

export const { setMovies, addToWatchLater } = movieSlice.actions;
export default movieSlice.reducer;
